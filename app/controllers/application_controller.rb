class ApplicationController < ActionController::Base

  def index
    @commits = CommitLog.commits
  end

  def receive_push
    CommitLog.process_post(params)
    CommitLog.commits.each do |commit|
      PrivatePub.publish_to "/pushes", "$.publish('/pushes', [ jQuery.parseJSON('#{commit.to_json}') ]);;"
    end
    # PrivatePub.publish_to "/pushes", "alert('#{CommitLog.commits.inspect}')"
    render :nothing => true
  end

end
