Feature: Users see a live log of github commits posted to this app

  @javascript
  Scenario: Someone just pushed to a github repo
    When I am viewing the live log
    And someone pushes to a github repo
    Then I should see the commits pop up