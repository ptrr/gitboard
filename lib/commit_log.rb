class CommitLog

  @@commits = []

  def self.process_post(post)
    if is_valid_post(post)
      self.reset
      post = JSON.parse(post["payload"])
      post["commits"].each do |commit|
        if is_valid_commit(commit)
          @@commits << commit
        end
      end unless post["commits"].nil?

    end
  end

  def self.commits
    @@commits
  end

  def self.reset
    @@commits = []
  end

  def self.is_valid_commit(commit)
    conditions = []
    conditions << ! commit["id"].nil?
    conditions << ! commit["url"].nil?
    conditions << ! commit["author"].nil?
    conditions << ! commit["message"].nil?
    conditions << ! commit["timestamp"].nil?
    conditions.all? {|c| c}
  end

  def self.is_valid_post(post)
    begin
      post = JSON.parse(post["payload"]) if post["payload"]
    rescue JSON::ParserError
      return false
    end

    conditions = []
    conditions << ! post["repository"].nil?
    conditions << ! post["before"].nil?
    conditions << ! post["after"].nil?
    conditions << ! post["commits"].nil?
    conditions << ! post["ref"].nil?
    conditions.all? {|c| c}
  end

end