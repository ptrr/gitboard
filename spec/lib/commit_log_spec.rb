require_relative "../../lib/commit_log"
require_relative "../../lib/notifier"
require 'json'

describe CommitLog do
  before(:each) do
    CommitLog.reset
  end

  it "stores commits from valid pushes" do
    CommitLog.process_post(VALID_POST)
    CommitLog.commits.count.should == 2
  end

  it "only keeps the most recent commits" do
    CommitLog.process_post(VALID_POST)
    CommitLog.commits.count.should == 2

    CommitLog.process_post(VALID_POST)
    CommitLog.commits.count.should == 2
  end

  it "doesn't do anything from nonsense posts" do
    CommitLog.process_post(INVALID_POST)
    CommitLog.commits.count.should == 0
  end

  it "lets me iterate over the most recently pushed commits" do
    CommitLog.process_post(VALID_POST)
    CommitLog.commits[0]["message"].should == "okay i give in"
    CommitLog.commits[1]["message"].should == "update pricing a tad"
  end
end

INVALID_POST = {"payload" => "hah"}

VALID_POST =   {"payload" => '{
  "before": "5aef35982fb2d34e9d9d4502f6ede1072793222d",
  "repository": {
    "url": "http://github.com/defunkt/github",
    "name": "github",
    "description": "You\'re lookin\' at it.",
    "watchers": 5,
    "forks": 2,
    "private": 1,
    "owner": {
      "email": "chris@ozmm.org",
      "name": "defunkt"
    }
  },
  "commits": [
    {
      "id": "41a212ee83ca127e3c8cf465891ab7216a705f59",
      "url": "http://github.com/defunkt/github/commit/41a212ee83ca127e3c8cf465891ab7216a705f59",
      "author": {
        "email": "chris@ozmm.org",
        "name": "Chris Wanstrath"
      },
      "message": "okay i give in",
      "timestamp": "2008-02-15T14:57:17-08:00",
      "added": ["filepath.rb"]
    },
    {
      "id": "de8251ff97ee194a289832576287d6f8ad74e3d0",
      "url": "http://github.com/defunkt/github/commit/de8251ff97ee194a289832576287d6f8ad74e3d0",
      "author": {
        "email": "chris@ozmm.org",
        "name": "Chris Wanstrath"
      },
      "message": "update pricing a tad",
      "timestamp": "2008-02-15T14:36:34-08:00"
    }
  ],
  "after": "de8251ff97ee194a289832576287d6f8ad74e3d0",
  "ref": "refs/heads/master"
}'}